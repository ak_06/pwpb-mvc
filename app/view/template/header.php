<?php

$url = $_SERVER['QUERY_STRING'];
$url = str_replace('url=', '', $url);

function linkActive($param) {
    global $url;
    if (str_contains($url, $param)) {
        return true;
    } else {
        return false;
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?= ASSETS; ?>/css/bootstrap.min.css">
    <title><?= $title ?></title>
</head>

<body>
    <nav class="navbar navbar-expand-lg bg-body-tertiary">
        <div class="container-fluid container">
            <a class="navbar-brand" href="#">Navbar</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <a class="nav-link <?= $url == "" ? "active" : "" ?>" href="<?= BASEURL;?>">Home</a>
                    <a class="nav-link <?= $url == "about" ? "active" : "" ?>" href="<?= BASEURL;?>/home/about">About</a>
                </div>
            </div>
        </div>
    </nav>