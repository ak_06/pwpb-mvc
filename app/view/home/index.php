<div class="container mt-5">
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Product Name</th>
                <th scope="col">Supplier ID</th>
                <th scope="col">Quantity</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($data as $d) : ?>
                <tr>
                    <th scope="row"><?= $d['ProductID'] ?></th>
                    <td><?= $d['ProductName'] ?></td>
                    <td><?= $d['SupplierID'] ?></td>
                    <td><?= $d['QuantityPerUnit'] ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

</div>