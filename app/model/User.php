<?php

class User
{
    private $db;

    public function __construct()
    {
        $db = new Database();

        $this->db = $db->accessDB();
    }

    public function register($email, $username, $password)
    {
        $query = "SELECT * FROM users WHERE email = :email";

        $stmt = $this->db->prepare($query);
        $stmt->bindParam(":email", $email);

        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            return false;
        }

        $hashedPassword = password_hash($password, PASSWORD_BCRYPT);
        $query = "INSERT INTO users(email, username, password) VALUES(:email, :username, :password)";
        $stmt = $this->db->prepare($query);
        $stmt->bindParam(":email", $email);
        $stmt->bindParam(":username", $username);
        $stmt->bindParam(":password", $hashedPassword);
        $stmt->execute();

        return true;
    }

    public function login($email, $password)
    {
        $query = "SELECT id, password FROM users WHERE email = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bindParam(":nama", $username);
    }
}
