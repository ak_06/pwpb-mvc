<?php

class Product {
    private $db;

    public function __construct()
    {
        $db = new Database();

        $this->db = $db->accessDB();
    }

    public function getAll() {
        $query = "SELECT * FROM products";
        $stmt = $this->db->query($query);

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function selectById($id)
    {
        $query = "SELECT * FROM products WHERE ProductID = :id";

        $stmt = $this->db->prepare($query);
        $stmt->bindParam(":id", $id);
        $stmt->execute();
        
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}