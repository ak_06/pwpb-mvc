<?php

class Database
{
    private $host = HOST;
    private $dbname = DBNAME;
    private $username = USERNAME;

    private $pdo;

    public function __construct()
    {
        try {
            $this->pdo = new PDO("mysql:host=$this->host;dbname=$this->dbname", $this->username, "");
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            return $e->getMessage();
        }
    }

    public function accessDB()
    {
        return $this->pdo;
    }
}
