<?php
require_once 'Constants.php';

class App
{
    protected $controller = 'home';
    protected $method = 'index';
    protected $params = [];

    public function __construct()
    {
        // var_dump(pathinfo("http://localhost".$_SERVER['REQUEST_URI']));
        $url = $this->parseURL();
        if (isset($url[0])) {
            if (file_exists("app/controller/$url[0].php")) {
                $this->controller = $url[0];
            }
        }

        require_once "app/controller/$this->controller.php";
        $this->controller = new $this->controller;
        

        if (isset($url[1])) {
            if (method_exists($this->controller, $url[1])) {
                $this->method = $url[1];
            }
        }

        if (!empty($url)) {
            unset($url[0], $url[1]);
            $this->params = array_values($url);
        }

        call_user_func_array([$this->controller, $this->method], $this->params);
    }

    public function parseURL()
    {
        // if (isset($_GET['url'])) {
        //     $url = $_GET['url'];
        //     $url = rtrim($url, '/');
        //     $url = filter_var($url, FILTER_SANITIZE_URL);
        //     $url = explode("/", $url);

        //     return $url;
        // }
        
        $url = $_SERVER['REQUEST_URI'];

        $url = trim($url, '/');
        $url = rtrim($url, '/');
        $url = filter_var($url, FILTER_SANITIZE_URL);
        $url = explode("/", $url);
        array_shift($url);

        return $url;
    }
}
