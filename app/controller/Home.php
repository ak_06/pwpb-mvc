<?php

class Home extends Controller
{
    public function index()
    {
        $data = $this->model('Product')->getAll();
        $this->view('Home', 'home/index', $data);
    }

    public function about()
    {
        $data = $this->model('Product')->selectById(2);
        $this->view('Create', 'home/select',$data);
    }
}
