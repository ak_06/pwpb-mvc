<?php

class Register extends Controller
{
    public function index()
    {
        $this->viewAuth("Register", "auth/register");
    }

    public function store()
    {
        $url = BASEURL;
        if (isset($_POST['register'])) {
            $username = $_POST['username'];
            $email = $_POST['email'];
            $password = $_POST['password'];

            $user = $this->model('User');

            if ($user->register($email, $username, $password)) {
                header("location: $url/login");
            } else {
                header("location: $url/register ");
            }
        }
    }
}