-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 30, 2023 at 07:04 AM
-- Server version: 8.0.30
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db-employee`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `CustomerID` varchar(10) NOT NULL,
  `CompanyName` varchar(255) NOT NULL,
  `ContactName` varchar(255) NOT NULL,
  `Address` text NOT NULL,
  `City` varchar(50) NOT NULL,
  `PostalCode` varchar(50) NOT NULL,
  `Country` varchar(50) NOT NULL,
  `Phone` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`CustomerID`, `CompanyName`, `ContactName`, `Address`, `City`, `PostalCode`, `Country`, `Phone`) VALUES
('ALFKI', 'Alfreds Futterkiste', 'Maria Anders', 'Obere Str. 57 ', 'Berlin', '12209', 'Germany', '030-0074321'),
('EASTC', 'Eastern Connection', 'Ann Devon', '35 King George', 'London', 'WX3FW', 'UK', '(171) 555-0297'),
('ISLAT', 'Island Trading ', 'Helen Bennett', '75 Crowther Way', 'Cowes', 'PO31 7PJ', 'UK', '(198) 555-8888'),
('MAISD', 'Maison Dewey', 'Catherine Dewey', 'Rue Joseph-Bens 532', 'Bruxulles', 'B-1180', 'Belgium', '(02) 201 24 67'),
('SEVES', 'Seven Seas Imports', 'Hari Kumar', '90 Wadhrust Rd.', 'London', 'OX154', 'UK', '(171) 555-1717');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `EmpId` int NOT NULL,
  `LastName` varchar(50) NOT NULL,
  `FirstName` varchar(50) NOT NULL,
  `Title` varchar(50) NOT NULL,
  `Address` text NOT NULL,
  `City` varchar(50) NOT NULL,
  `Country` varchar(50) NOT NULL,
  `HomePhone` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`EmpId`, `LastName`, `FirstName`, `Title`, `Address`, `City`, `Country`, `HomePhone`) VALUES
(1, 'Davolio', 'Nancy', 'Sales Rep.', '507 - 20th Ave. E. Apt.2A', 'Seattle', 'USA', '(206) 555-9857'),
(2, 'Fuller', 'Andrew', 'Vice President', '908 W. Capital Way', 'Tacoma', 'USA', '(206) 555-9857'),
(3, 'Leverling', 'Janet', 'Sales Rep.', '722 Moss Bay Blvd.', 'Kirkland', 'USA', '(206) 555-3412'),
(4, 'Peacock', 'Margaret', 'Sales Rep.', '4110 Old Redmond Rd.', 'Redmond', 'USA', '(206) 555-8122'),
(5, 'Buchanan', 'Steven', 'Sales Manager', '14 Garrett Hill', 'London', 'UK', '(71) 555-4848'),
(6, 'Suyama', 'Michael', 'Sales Rep.', 'Coventry House Miner Rd.', 'London', 'UK', '(71) 555-7773'),
(7, 'King', 'Robert', 'Sales Rep.', '12 Winchester Way', 'London', 'UK', '(71) 555-5598'),
(8, 'Callahan', 'Laura', 'Sales Coord.', '42276 - 11th Ave. N.E.', 'Seattle', 'USA', '(206) 555-1189'),
(9, 'Dodsworth', 'Anne', 'Sales Rep.', '7 Houndstooth Rd.', 'London', 'UK', '(71) 555-4444');

-- --------------------------------------------------------

--
-- Table structure for table `orderdetails`
--

CREATE TABLE `orderdetails` (
  `OrderID` int NOT NULL,
  `ProductID` int NOT NULL,
  `UnitPrice` double NOT NULL,
  `Quantity` int NOT NULL,
  `Discount` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orderdetails`
--

INSERT INTO `orderdetails` (`OrderID`, `ProductID`, `UnitPrice`, `Quantity`, `Discount`) VALUES
(10256, 53, 32.8, 15, 0),
(10256, 77, 13, 12, 0),
(10257, 27, 43.9, 25, 0),
(10257, 39, 18, 6, 0),
(10257, 77, 13, 15, 0),
(10258, 2, 19, 50, 0.2),
(10258, 5, 21.35, 65, 0.2),
(10258, 32, 32, 6, 0.2),
(10259, 21, 10, 10, 0),
(10259, 37, 26, 1, 0),
(10260, 41, 9.65, 16, 0.25),
(10260, 57, 19.5, 50, 0),
(10260, 62, 49.3, 15, 0.25),
(10260, 70, 15, 21, 0.25);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `OrderID` int NOT NULL,
  `CustID` varchar(10) NOT NULL,
  `EmpID` int NOT NULL,
  `OrderDate` varchar(30) NOT NULL,
  `RequiredDate` varchar(30) NOT NULL,
  `ShippedDate` varchar(30) NOT NULL,
  `ShipVia` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`OrderID`, `CustID`, `EmpID`, `OrderDate`, `RequiredDate`, `ShippedDate`, `ShipVia`) VALUES
(10256, 'EASTC', 3, '8/15/1994', '9/12/1994', '8/17/1994', 2),
(10257, 'SEVES', 4, '8/16/1994', '9/13/1994', '8/22/1994', 3),
(10258, 'MAISD', 1, '8/16/1994', '9/14/1994', '8/23/1994', 1),
(10259, 'ALFKI', 4, '8/18/1994', '9/15/1994', '8/25/1994', 3),
(10260, 'ISLAT', 4, '8/19/1994', '9/16/1994', '8/29/1994', 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `ProductID` int NOT NULL,
  `ProductName` varchar(255) NOT NULL,
  `SupplierID` int NOT NULL,
  `QuantityPerUnit` varchar(255) NOT NULL,
  `UnitPrice` double NOT NULL,
  `UnitsInStock` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`ProductID`, `ProductName`, `SupplierID`, `QuantityPerUnit`, `UnitPrice`, `UnitsInStock`) VALUES
(2, 'Chang', 1, '24 - 12 oz bottles', 19, 17),
(5, 'Chef Anton\'s Gumbo Mix', 2, '36 boxes', 21.35, 50),
(21, 'Sir Rodney\'s Scones', 8, '24 pkgs. x 4 pieces', 10, 17),
(27, 'Schoggi Schokolade', 11, '100 - 100 g pieces', 43.9, 49),
(32, 'Mascarpone Fabioli', 14, '24 - 200 g pkgs. ', 32, 9),
(37, 'Gravad lax', 17, '12 - 500 g pgks.\r\n', 26, 11),
(39, 'Chartreuse Verte', 18, '750 cc per bottle', 18, 69),
(41, 'Jack\'s Clam Chowder', 19, '12 - 12 oz cans', 9.65, 85),
(53, 'Perth Pasties', 24, '48 pieces', 32.8, 15),
(57, 'Ravioli Angelo', 26, '24 - 250 g pkgs.', 19.5, 36),
(62, 'Tarte au sucre', 29, '48 pies', 49.3, 17),
(65, 'Hot Pepper Sauce', 2, '32 - 8 oz bottles.', 21.05, 76),
(70, 'Outback Lager', 7, '23 - 355 ml bottles', 15, 15),
(74, 'Longlife Tofu', 4, '5 kg pkg.', 10, 4),
(77, 'Original Frankfurter', 12, '12 boxes', 13, 32);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `username`, `password`) VALUES
(1, 'senamashira@gmail.com', 'sena', '$2y$10$wqidPix.qrGhf51l9lJWjO43qoQEc0to9oqHCobojrOX/NjbKe8/u');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`CustomerID`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`EmpId`);

--
-- Indexes for table `orderdetails`
--
ALTER TABLE `orderdetails`
  ADD KEY `OrderID` (`OrderID`,`ProductID`),
  ADD KEY `UnitPrice` (`UnitPrice`),
  ADD KEY `ProductID` (`ProductID`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`OrderID`),
  ADD KEY `CustID` (`CustID`,`EmpID`),
  ADD KEY `EmpID` (`EmpID`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`ProductID`),
  ADD KEY `UnitPrice` (`UnitPrice`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `EmpId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orderdetails`
--
ALTER TABLE `orderdetails`
  ADD CONSTRAINT `orderdetails_ibfk_2` FOREIGN KEY (`OrderID`) REFERENCES `orders` (`OrderID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `orderdetails_ibfk_4` FOREIGN KEY (`ProductID`) REFERENCES `products` (`ProductID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `orderdetails_ibfk_5` FOREIGN KEY (`UnitPrice`) REFERENCES `products` (`UnitPrice`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`CustID`) REFERENCES `customers` (`CustomerID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`EmpID`) REFERENCES `employees` (`EmpId`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
